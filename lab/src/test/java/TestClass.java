import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestClass {

    /**
     * Write your tests here.
     * Don't forget to put @Test before your test method.
     * */

    static final String universityName = "Politecnico di Torino";
	private final static int MISSING=-1; // predefined 'int' return values in initial implementation
	private University poli;
	
	@Before
	public void setUp() throws Exception { // FIXTURE, 
		// the name 'setUp' is a convention deriving from previous JUnit versions
		poli = new University(universityName);
		poli.setRector("Guido", "Saracco");
	}

	@Test
	public void testNameRector(){
		String name = poli.getName();
		String rector = poli.getRector();
		
		assertContained("Wrong rector name","Saracco",rector);
	}


	@Test
	public void testStudents(){				
		int s1 = poli.enroll("Vilfredo","Pareto");
		int s2 = poli.enroll("Galileo","Ferraris");
		int s3 = poli.enroll("Leo", "Da Vinci");
	
		String ss1 = poli.student(s1);
		assertNotNull("Missing student info", ss1);

	}
	
	@Test
	public void testCourses(){
		int macro = poli.activate("Macro Economics", "Paul Krugman");
		int oop = poli.activate("Object Oriented Programming", "James Gosling");
		
		assertNotNull("Missing course description",poli.course(macro));
	
	}

	
	
	// -------------------- Utility (or Helper) methods ------------------------------------------
	
	// Specialised assert methods, make the test methods easier to read
	
	/**
	 * Assert that a sub string is contained in the actual string
	 * 
	 * @param expectedSubStr the expected sub string
	 * @param actualStr      the actual string
	 */
	private static void assertContained(String expectedSubStr, String actualStr) {
		assertContained(null,expectedSubStr,actualStr);
	}
	
	
	/**
	 * Assert that a sub string is contained in the actual string
	 * 
	 * @param expectedSubStr the expected sub string
	 * @param actualStr      the actual string
	 */
	private static void assertContained(String msg, String expectedSubStr, String actualStr) {
		assertTrue((msg==null?"":msg+": ") + "expected sub string [" + expectedSubStr + "] is not contained in ["+actualStr+"]",
					(actualStr==null?false:actualStr.contains(expectedSubStr)));
	}
	
	// other support methods
	
	private static int countLines(String s) {
		if(s==null) return 0;
		return 1 + s.trim().replaceAll("[^\n]", "").length();
	}

}
